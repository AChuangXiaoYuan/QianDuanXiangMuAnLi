//配置
window['ctx'] = "http://127.0.0.1:88/admin";
var app = {
    init: function () {
        //初始化全局Ajax
        this._initAjax();

        return this;

    },
    _initAjax: function () {
        $(document).ajaxSend(function (evt, request, settings) {// 请求开始

        }).ajaxComplete(function (event, request, settings, xhr) {
            if (settings.url.indexOf(".html") > 0) {

            } else {
                // var res = JSON.parse(request.responseText);
                // if (res.code == 1003) { //自定义权限异常
                //     alert(settings.url + "\n" + res.ResponseMessage);
                //     return;
                // }
            }
        }).ajaxError(function (event, request, settings) {
            app.tips("请求出现异常, 可能是接口服务没有启动!", 0);
        });
    },
    tips: function (text, type) {
        var css = '', ico = 'ok';
        if (type == 0) {
            css = 'tips-error';
            ico = 'error';
        }
        $("body").append('<div class="tips ' + css + '"><img src="static/icon/' + ico + '.svg"/>' + text + '</div>');
        $("div.tips").animate({top: "20px", opacity: 1}, 200, function () {
            setTimeout(function () {
                $("div.tips").animate({opacity: 0}, 200, function () {
                    $("div.tips").remove();
                });
            }, 1700);
        });
    },
    serializeObject: function (frmObj) {
        var data = frmObj.serializeArray();
        var o = {};
        $.each(data, function (i, obj) {
            o[obj.name] = obj.value;
        });
        return o;
    },
    fullForm: function(frmObj, jsonData){
        $.each(jsonData, function (name, ival) {
           // var $oinput = frmObj.find("input:[name=" + name + "]");
            frmObj.find("[name="+name+"]").val(ival);
        });
    }
}

app.init();